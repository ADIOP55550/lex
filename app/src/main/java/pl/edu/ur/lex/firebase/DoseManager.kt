package pl.edu.ur.lex.firebase

import android.security.keystore.UserNotAuthenticatedException
import android.util.Log
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import pl.edu.ur.lex.data.model.*
import pl.edu.ur.lex.firebase.DatabaseManager.database
import java.time.DayOfWeek
import java.util.*
import java.util.function.Function

object DoseManager {
    fun forMedicine(medicine: Medicine): Repository<Dose, DoseId, String> {
        return DoseManagerReal(medicine)
    }
}

class DoseManagerReal(private val forMedicine: Medicine) : Repository<Dose, DoseId, String>() {
    private val database = DatabaseManager.database


    override fun findAll(): Task<Collection<Dose>> {
        return forCurrentUser { user ->
            val dosesPathForUser =
                DatabaseManager.dosesPathForUserAndMedicine(user.uid, forMedicine.id)
            Log.d("log", dosesPathForUser)
            database.getReference(dosesPathForUser).get()
                .onSuccessTask {
                    Tasks.forResult(it.children.mapNotNull { data ->
                        val dose: Dose?

                        try {
                            println(data.child("timeBegin"))
                            dose = data.getValue(Dose::class.java)
                        } catch (e: Exception) {
                            Log.e("ERROR GETTING MEDICINE", e.toString())
                            e.printStackTrace()
                            throw e
                        }

                        Log.d("got dose", dose!!.toString())
                        dose
                    }
                        .map { mapToRealDose(it, forMedicine) }
                    )
                }
        }
    }

    override fun findOne(id: DoseId): Task<Optional<Dose>> {
        return forCurrentUser { user ->
            database.getReference(DatabaseManager.dosePathForUserAndMedicine(
                user.uid,
                forMedicine.id,
                id
            ))
                .get()
                .onSuccessTask {
                    Tasks.forResult(Optional.ofNullable(it.getValue(Dose::class.java))
                        .map { mapToRealDose(it, forMedicine) })
                }
        }
    }

    override fun findMany(value: String): Task<Collection<Dose>> {
        return forCurrentUser { user ->
            database.getReference(DatabaseManager.dosesPathForUserAndMedicine(user.uid,
                forMedicine.id)).get()
                .onSuccessTask {
                    Tasks.forResult(it.children.mapNotNull { data -> data.getValue(Dose::class.java) }
                        .map { mapToRealDose(it, forMedicine) })
                }
        }
    }

    override fun create(value: Dose): Task<Dose> {
        println("Creating dose for medicine @" + forMedicine.id)
        println(value)
        val ref = getNewDoseRefForMedicine(forMedicine.id)

        value.id = ref.key.toString()

        println(value)

        ref.setValue(value)

        return Tasks.forResult(value)
    }

    override fun update(value: Dose): Task<Dose> {
        return forCurrentUser {
            val ref = database.getReference(DatabaseManager.dosePathForUserAndMedicine(
                it.uid,
                forMedicine.id,
                value.id
            ))
            ref.setValue(value).onSuccessTask {
                Tasks.forResult(value)
            }
        }
    }

    override fun createOrUpdate(value: Dose): Task<Dose> {
        return if (value.id.isNotEmpty())
            update(value)
        else
            create(value)
    }


    override fun delete(value: Dose): Task<Void> {
        return forCurrentUser {
            val ref = database.getReference(DatabaseManager.dosePathForUserAndMedicine(
                it.uid,
                forMedicine.id,
                value.id
            ))
            ref.removeValue()
        }
    }


    companion object {
        private fun <T> forCurrentUser(action: Function<FirebaseUser, T>): T {
            DatabaseManager.auth.currentUser.let { user ->
                if (user == null) {
                    val err =
                        UserNotAuthenticatedException("Cannot access doses of not logged in user")
                    @Suppress("UNCHECKED_CAST")
                    return Tasks.forException<UserNotAuthenticatedException>(err) as T
                }

                return action.apply(user)
            }

        }

        fun mapToRealDose(value: Dose, parent: Medicine): Dose {
            value.medicine = parent
            if (value.repeats) {
                value._repetitionStrategyData = when (value.repetitionStrategy) {
                    RepetitionStrategy.EVERY_N_PERIODS -> {
                        println(value._repetitionStrategyData)
                        println(value._repetitionStrategyData?.javaClass?.name)
                        val m = value._repetitionStrategyData as Map<*, *>

                        (m["first"] as Long) to (RepetitionPeriod.valueOf(m["second"] as String))
                    }
                    RepetitionStrategy.WEEKDAYS -> {
                        (value._repetitionStrategyData as List<*>).map { v ->
                            DayOfWeek.valueOf(v.toString())
                        }
                    }
                    RepetitionStrategy.EVERYDAY -> value._repetitionStrategyData
                }

                value._repetitionEndStrategyData = when (value.repetitionEndStrategy) {
                    RepetitionEndStrategy.INFINITE -> value._repetitionEndStrategyData
                    RepetitionEndStrategy.N_TIMES -> (value._repetitionEndStrategyData as Long).toInt()
                    RepetitionEndStrategy.UNTIL_DATE -> {
                        println(value._repetitionEndStrategyData)
                        println(value._repetitionEndStrategyData?.javaClass?.name)

                        val d = value._repetitionEndStrategyData as HashMap<String, Int>
                        @Suppress("DEPRECATION")
                        Date(d.getOrDefault("year", 0),
                            d.getOrDefault("month", 0),
                            d.getOrDefault("date", 1))
                    }
                }
            }

            return value
        }

        fun getNewDoseRefForMedicine(medicineId: String): DatabaseReference {
            return forCurrentUser {
                database.getReference(DatabaseManager.dosesPathForUserAndMedicine(it.uid,
                    medicineId))
                    .push()
            }
        }
    }
}
