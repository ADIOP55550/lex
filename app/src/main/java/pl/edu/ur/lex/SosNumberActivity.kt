package pl.edu.ur.lex

import android.content.SharedPreferences
import android.os.Bundle
import android.text.InputType
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.ui.AppBarConfiguration
import pl.edu.ur.lex.databinding.ActivitySosNumberBinding
import pl.edu.ur.lex.firebase.DatabaseManager

class SosNumberActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivitySosNumberBinding
    private lateinit var sosNumberPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySosNumberBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)

        binding.chageSosNumber.isCursorVisible = true
        binding.chageSosNumber.isFocusableInTouchMode = true
        binding.chageSosNumber.inputType = InputType.TYPE_CLASS_PHONE

        val userid = DatabaseManager.auth.uid.toString()

        sosNumberPreferences = getSharedPreferences("MySoSNumber$userid", MODE_PRIVATE)
        val editSosNumber: SharedPreferences.Editor = sosNumberPreferences.edit()

        binding.chageSosNumber.setText(sosNumberPreferences.getString("Number",
            ConfigurationManager.sosNumber))

        binding.saveSosNumber.setOnClickListener {
            if (binding.chageSosNumber.text.isNullOrEmpty()) {
                binding.chageSosNumber.error = "Cannot be blank"
                return@setOnClickListener
            }

            val tempNumber = sosNumberPreferences.getString("Number",
                ConfigurationManager.sosNumber)
            editSosNumber.remove(tempNumber).apply()
            ConfigurationManager.sosNumber = binding.chageSosNumber.text.toString()

            editSosNumber.putString("Number", ConfigurationManager.sosNumber).apply()
            Toast.makeText(this, "sos number changed", Toast.LENGTH_SHORT).show()
        }
    }
}