package pl.edu.ur.lex

//import com.google.firebase.database.DatabaseReference
//import com.google.firebase.ktx.Firebase

//private lateinit var database: DatabaseReference
import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.gms.tasks.Task
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import pl.edu.ur.lex.data.model.Dose
import pl.edu.ur.lex.data.model.Medicine
import pl.edu.ur.lex.databinding.FragmentFirstBinding
import pl.edu.ur.lex.doseList.DoseListAdapter
import pl.edu.ur.lex.firebase.DatabaseManager
import pl.edu.ur.lex.firebase.MedicineManager
import pl.edu.ur.lex.medicines.MedicineRecyclerViewAdapter
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment() {

    private var binding: FragmentFirstBinding? = null
    private lateinit var medicineRecyclerViewAdapter: MedicineRecyclerViewAdapter
    private lateinit var dosesRecyclerViewAdapter: DoseListAdapter

    private val medicines: MutableList<Medicine> = mutableListOf()
    private val doses: MutableList<Dose> = mutableListOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {

        binding = FragmentFirstBinding.inflate(inflater, container, false)

        DatabaseManager.auth.addAuthStateListener {
            if (binding != null)
                binding!!.userLoggedName.text = Optional.ofNullable(it.currentUser)
                    .map { u -> getString(R.string.logged_in_as, u.email) }
                    .orElse(getString(R.string.not_logged_in))
        }

        DatabaseManager.auth.currentUser?.email?.let {
            binding!!.userLoggedName.text =
                getString(R.string.logged_in_as, it)
        }

        binding!!.dosesForDayLabel.text = getString(R.string.doses_for_day,
            LocalDate.now().format(
                DateTimeFormatter.ISO_LOCAL_DATE))


        return binding!!.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = binding!!
        binding.mainCalendar.setOnDateChangeListener { _, year, month, dayOfMonth ->
            val date = LocalDate.of(year, month + 1, dayOfMonth)
            binding.dosesForDayLabel.text = getString(R.string.doses_for_day,
                date.format(
                    DateTimeFormatter.ISO_LOCAL_DATE))

            loadDosesForDate(date)
        }

        createAdapters()

        refreshAllMedicines().continueWith {

            loadDosesForDate(LocalDate.now())

            DatabaseManager.database.getReference(DatabaseManager.medicinesPathForUser(
                DatabaseManager.auth.currentUser!!.uid))
                .addChildEventListener(object : ChildEventListener {
                    override fun onChildAdded(
                        snapshot: DataSnapshot,
                        previousChildName: String?,
                    ) {
                        refreshAllMedicines()
                    }

                    override fun onChildChanged(
                        snapshot: DataSnapshot,
                        previousChildName: String?,
                    ) {
                        refreshAllMedicines()
                    }

                    override fun onChildRemoved(snapshot: DataSnapshot) {
                        refreshAllMedicines()
                    }

                    override fun onChildMoved(
                        snapshot: DataSnapshot,
                        previousChildName: String?,
                    ) {
                        refreshAllMedicines()
                    }

                    override fun onCancelled(error: DatabaseError) {
                        error.toException().printStackTrace()
                    }
                })
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun loadDosesForDate(date: LocalDate): Task<Collection<Medicine>> {
        println("FirstFragment.loadDosesForDate")
        println("date = [${date}]")

        return MedicineManager.findAll().addOnSuccessListener {
            val todayDoses = medicines
                .flatMap { m -> m.doses.entries }
                .filter { entry ->
                    val dose = entry.value
                    try {
                        dose.getTimesAfterOrEqual(date)
                            .map { v -> println(v); v }
                            .firstOrNull()
                            ?.toLocalDate()
                            ?.compareTo(date) == 0
                    } catch (e: Exception) {
                        e.printStackTrace()
                        throw e
                    }
                }
                .sortedByDescending { entry -> entry.value.dateBegin }

            doses.clear()
            doses.addAll(todayDoses.map { it.value }.toMutableList())

            println("doses = ${doses.map { it.toString() }.reduceOrNull(String::plus)}")

            dosesRecyclerViewAdapter.selectedDate =
                Date(date.year - 1900, date.monthValue - 1, date.dayOfMonth)

            dosesRecyclerViewAdapter.notifyDataSetChanged()
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun refreshAllMedicines(): Task<Collection<Medicine>> {
        println("FirstFragment.refreshAll")
        return MedicineManager.findAll().addOnSuccessListener {
            if (binding == null)
                return@addOnSuccessListener
            medicines.clear()
            medicines.addAll(it.toMutableList())
            medicineRecyclerViewAdapter.notifyDataSetChanged()
        }
    }

    private fun createAdapters() {
        medicineRecyclerViewAdapter = MedicineRecyclerViewAdapter(medicines)
        binding!!.listMedicines.adapter = medicineRecyclerViewAdapter
        dosesRecyclerViewAdapter = DoseListAdapter(doses, Date())
        binding!!.listDoses.adapter = dosesRecyclerViewAdapter
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

}