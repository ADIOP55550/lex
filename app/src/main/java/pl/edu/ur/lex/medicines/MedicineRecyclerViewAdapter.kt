package pl.edu.ur.lex.medicines

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import pl.edu.ur.lex.data.model.Medicine
import pl.edu.ur.lex.databinding.MedicineFragmentItemBinding
import java.util.*

/**
 * [RecyclerView.Adapter] that can display a [Medicine].
 */
class MedicineRecyclerViewAdapter(
    private val values: List<Medicine>,
) : RecyclerView.Adapter<MedicineRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            MedicineFragmentItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        ) {
            val i = Intent(parent.context, MedicineEditActivity::class.java)
            i.putExtra("medicine", it)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            parent.context.startActivity(i)
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(
        binding: MedicineFragmentItemBinding,
        private val clickHandler: (Medicine) -> Unit,
    ) :
        RecyclerView.ViewHolder(binding.root) {
        val medicineName: TextView = binding.medicineName
        val description: TextView = binding.description
        val rightLabel: TextView = binding.rightLabel
        var itemValue: Medicine? = null

        override fun toString(): String {
            return super.toString() + " '" + medicineName.text + "'"
        }

        init {
            binding.root.setOnClickListener {
                Objects.requireNonNull(itemValue)
                clickHandler(itemValue!!)
            }
        }

        fun bind(item: Medicine) {
            medicineName.text = item.name
            description.text = item.description
            rightLabel.text =
                item.doses.size.let { it.toString() + " dose" + (if (it != 1) "s" else "") }

            itemValue = item
        }
    }

}