package pl.edu.ur.lex

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import coil.load
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.*
import pl.edu.ur.lex.adapter.ChatAdapter
import pl.edu.ur.lex.data.model.Chat
import pl.edu.ur.lex.data.model.User
import pl.edu.ur.lex.databinding.ActivityChatBinding
import java.time.LocalTime
import java.time.format.DateTimeFormatter


class ChatActivity : AppCompatActivity() {

    private lateinit var binding: ActivityChatBinding
    private lateinit var userImage: String
    private lateinit var manager: LinearLayoutManager
    var firebaseUser: FirebaseUser? = null
    var reference: DatabaseReference? = null

    var chatList = ArrayList<Chat>()
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        binding = ActivityChatBinding.inflate(layoutInflater)
        setContentView(binding.root)

        manager = LinearLayoutManager(this)
        manager.stackFromEnd = true

        binding.chatRecyclerViewActivity.layoutManager = manager

        val userId = intent.getStringExtra("userId")
        val userName = intent.getStringExtra("userName")
        userImage = intent.getStringExtra("userImage")!!

        binding.imgBack.setOnClickListener {
            onBackPressed()
        }

        firebaseUser = FirebaseAuth.getInstance().currentUser!!
        reference = FirebaseDatabase.getInstance().getReference("users")


        reference!!.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                val user = snapshot.child(userId.toString()).getValue(User::class.java)

                binding.tvUserName.text = user!!.userName
                binding.imgProfile.load(user.profileImage)
            }
        })

        binding.btnSendMessage.setOnClickListener {
            val message: String = binding.etMessage.text.toString()

            val formatter = DateTimeFormatter.ofPattern("HH:mm")
            val time: String = LocalTime.now().format(formatter)


            if (message.isBlank()) {
                Toast.makeText(applicationContext, "message is empty", Toast.LENGTH_SHORT).show()
                binding.etMessage.setText("")
            } else {
                if (userId != null) {
                    sendMessage(firebaseUser!!.uid, userId, message, time)
                }
                binding.etMessage.setText("")
            }
        }

        if (userId != null) {
            readMessage(firebaseUser!!.uid, userId)
        }

    }

    private fun sendMessage(senderId: String, receiverId: String, message: String, time: String) {
        val reference: DatabaseReference? = FirebaseDatabase.getInstance().reference

        val hashMap: HashMap<String, String> = HashMap()
        hashMap["senderId"] = senderId
        hashMap["receiverId"] = receiverId
        hashMap["message"] = message
        hashMap["time"] = time

        reference!!.child("Chat").push().setValue(hashMap)

    }

    private fun readMessage(senderId: String, receiverId: String) {
        val databaseReference: DatabaseReference =
            FirebaseDatabase.getInstance().getReference("Chat")

        databaseReference.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                TODO("Not yet implemented")
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                chatList.clear()
                for (dataSnapShot: DataSnapshot in snapshot.children) {
                    val chat = dataSnapShot.getValue(Chat::class.java)

                    if (chat!!.senderId == senderId && chat.receiverId == receiverId ||
                        chat.senderId == receiverId && chat.receiverId == senderId
                    ) {
                        chatList.add(chat)
                    }
                }

                val chatAdapter = ChatAdapter(this@ChatActivity, chatList)

                binding.chatRecyclerViewActivity.adapter = chatAdapter
            }
        })
    }
}
