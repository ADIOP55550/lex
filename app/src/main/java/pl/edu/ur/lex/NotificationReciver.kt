package pl.edu.ur.lex

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.media.AudioManager.*
import android.media.MediaPlayer
import android.media.RingtoneManager
import android.net.Uri
import androidx.core.app.NotificationCompat

class NotificationReciver : BroadcastReceiver() {

    private val notificationID = 1
    private val channelID = "channel1"
    private val titleExtra = "titleExtra"
    private val messageExtra = "messageExtra"



    override fun onReceive(context: Context, intent: Intent) {

//        val reminderIntent = Intent(context, ReminderReceiver::class.java)
//        reminderIntent.putExtra("titleExtra", titleExtra)
//        reminderIntent.putExtra("messageExtra", messageExtra)
//        val reminderPendingIntent = PendingIntent.getBroadcast(
//            context,
//            notificationID,
//            reminderIntent,
//            PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE

//        )

        val notification = NotificationCompat.Builder(context, channelID)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentTitle(intent.getStringExtra(titleExtra))
            .setContentText(intent.getStringExtra(messageExtra))
            .setSound(Uri.parse("android.resource://"+context.packageName +"/"+R.raw.powiadomienie))
            .build()
        val manager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        manager.notify(notificationID, notification)
    }
}