package pl.edu.ur.lex.data.model

import java.io.Serializable
import java.time.LocalTime

data class Time(
    val hour: Int = 0,
    val minute: Int = 0,
    val second: Int = 0,
    val nanoOfSecond: Int = 0,
) : Serializable {
    fun toLocalTime(): LocalTime {
        return LocalTime.of(hour, minute, second, nanoOfSecond)
    }

    companion object {
        fun fromLocalTime(lt: LocalTime): Time {
            return Time(lt.hour, lt.minute, lt.second, lt.nano)
        }

        val NOON = Time.fromLocalTime(LocalTime.NOON)
        val MIDNIGHT = Time.fromLocalTime(LocalTime.MIDNIGHT)
        val MIN = Time.fromLocalTime(LocalTime.MIN)
        val MAX = Time.fromLocalTime(LocalTime.MAX)
    }
}
