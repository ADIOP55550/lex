package pl.edu.ur.lex.medicines

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await
import pl.edu.ur.lex.DoseRecyclerViewAdapter
import pl.edu.ur.lex.NotificationHelper
import pl.edu.ur.lex.R
import pl.edu.ur.lex.data.model.Dose
import pl.edu.ur.lex.data.model.Medicine
import pl.edu.ur.lex.databinding.FragmentMedicineEditBinding
import pl.edu.ur.lex.exceptions.ConflictException
import pl.edu.ur.lex.exceptions.EmptyInputException
import pl.edu.ur.lex.firebase.DoseManagerReal
import pl.edu.ur.lex.firebase.MedicineManager
import java.lang.NullPointerException
import java.time.LocalDate
import java.util.*
import kotlin.streams.toList

private const val ARG_MEDICINE = "medicine"

/**
 * A simple [Fragment] subclass.
 * Use the [MedicineEditFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class MedicineEditFragment : Fragment() {
    private var binding: FragmentMedicineEditBinding? = null

    private var medicine: Medicine? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            medicine = it.getSerializable(ARG_MEDICINE) as Medicine?
        }
        if (requireActivity().intent.hasExtra("medicine")) {
            val medicine = requireActivity().intent.getSerializableExtra("medicine") as Medicine?

            if (medicine != null) this.medicine = medicine
        }

        if (medicine == null)
            medicine = Medicine()


        doseRecyclerViewAdapter = DoseRecyclerViewAdapter(dosesPairsList, onDelete = {
            val i = dosesPairsList.indexOf(it)
            dosesPairsList.removeAt(i)
            doseRecyclerViewAdapter.notifyItemRemoved(i)
            println(it.toString())
        }, onEditStart = {
            println("EDIT START OF")
            println(it)

            @Suppress("USELESS_CAST")
            findNavController().navigate(R.id.action_medicineEdit_to_doseEditFragment, bundleOf(
                "medicine" to medicine as Medicine,
                "dose" to it.second as Dose,
            ))
        })

        binding?.recyclerViewDoses?.adapter = doseRecyclerViewAdapter
    }

    private lateinit var doseRecyclerViewAdapter: DoseRecyclerViewAdapter


    private var dosesPairsList: MutableList<Pair<String, Dose>> = mutableListOf()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {

        binding = FragmentMedicineEditBinding.inflate(inflater, container, false)
        val view = binding!!.root

        Objects.requireNonNull(binding)
        val binding = binding!!

        if (medicine == null) {
            medicine = Medicine()
        }

        val medicine = medicine!!

        println("Started with medicine ===")
        println(medicine)
        dosesPairsList = medicine.doses.entries.map { it.toPair() }.toMutableList()
        binding.editMedicineName.setText(medicine.name)
        binding.editMedicineDescription.setText(medicine.description)

        binding.buttonSave.setOnClickListener {
            lifecycleScope.launch {
                try {
                    saveMedicine()
                } catch (e: ConflictException) {
                    e.printStackTrace()
                    return@launch
                } catch (e: EmptyInputException) {
                    e.printStackTrace()
                    return@launch
                }


                if (findNavController().previousBackStackEntry != null)
                    findNavController().navigateUp()
                else
                    activity?.finish()
            }
        }

        findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData<Pair<String, Dose>?>(
            "dose")
            ?.observe(viewLifecycleOwner) {
                println("GOT DOSE")
                println(it.toString())

                if (it == null)
                    return@observe

                doseAdded(it)
            }

        findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData<Pair<String, Dose>?>(
            "doseEdit")
            ?.observe(viewLifecycleOwner) {
                println("GOT DOSE EDITED")
                println(it.toString())

                if (it == null)
                    return@observe

                doseEdited(it)
            }

        binding.button4.setOnClickListener {
            findNavController().navigate(R.id.action_medicineEdit_to_doseEditFragment, bundleOf(
                "medicine" to medicine,
                "dose" to null
            ))
        }

        binding.editMedicineName.let { editText ->

            editText.addTextChangedListener {
                if (it.isNullOrEmpty()) {
                    editText.error = "Medicine name cannot be empty"
                    return@addTextChangedListener
                }

                medicine.name = it.toString()
            }
        }

        binding.editMedicineDescription.addTextChangedListener {
            medicine.description = it.toString()
        }


        return view
    }

    private fun doseAdded(
        pair: Pair<String, Dose>,
    ) {
        println("DOSE ADDED")
        println(pair.first)
        println(pair.second.toString())

        Objects.requireNonNull(medicine)

        val newId = DoseManagerReal.getNewDoseRefForMedicine(medicine!!.id).key

        println("newId = ${newId}")
        if (newId == null) throw NullPointerException("newId")

        pair.second.id = newId

        medicine!!.doses[newId] = pair.second

        dosesPairsList.add(newId to pair.second)

        println("GOT AFTER KEY")
        println(pair.toString())
        doseRecyclerViewAdapter.notifyItemInserted(dosesPairsList.size - 1)
    }

    private fun doseEdited(
        pair: Pair<String, Dose>,
    ) {
        println("DOSE EDITED")
        println(pair.first)
        println(pair.second.toString())

        Objects.requireNonNull(medicine)
        val i = dosesPairsList.indexOfFirst { pair2 -> pair2.first == pair.first }

        println(i)
        println(dosesPairsList)

        medicine!!.doses[pair.first] = pair.second

        dosesPairsList[i] = pair
        doseRecyclerViewAdapter.notifyItemChanged(i)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = binding!!

        doseRecyclerViewAdapter = DoseRecyclerViewAdapter(dosesPairsList, onDelete = {
            val i = dosesPairsList.indexOf(it)
            dosesPairsList.removeAt(i)
            doseRecyclerViewAdapter.notifyItemRemoved(i)
            println(it.toString())
        }, onEditStart = {
            println("EDIT START OF")
            println(it)
            @Suppress("USELESS_CAST")
            findNavController().navigate(R.id.action_medicineEdit_to_doseEditFragment, bundleOf(
                "medicine" to medicine as Medicine,
                "dose" to it.second as Dose,
            ))
        })

        binding.recyclerViewDoses.adapter = doseRecyclerViewAdapter

    }

    private suspend fun saveMedicine() {
        if (medicine == null)
            medicine = Medicine()

        Objects.requireNonNull(binding)
        val binding = binding!!

        val nameEdit = binding.editMedicineName
        val descriptionEdit = binding.editMedicineDescription

        if (nameEdit.text.isNullOrBlank()) {
            nameEdit.error = "Name cannot be empty"
            throw EmptyInputException("Name cannot be empty")
        }
        val medicineName = nameEdit.text.toString()

        if (medicine?.id.isNullOrEmpty())
            if (MedicineManager.findMany(medicineName).await().stream().peek { println(it) }
                    .toList()
                    .isNotEmpty()
            ) {
                nameEdit.error = "Medicine with this name already exists!"
                throw ConflictException("Medicine with this name already exists!")
            }

        val medicine = medicine!!

        medicine.doses = dosesPairsList.toMap().toMutableMap()

        medicine.name = medicineName
        medicine.description =
            Optional.ofNullable(descriptionEdit.text).map { it.toString() }.orElse("")

        MedicineManager.createOrUpdate(medicine)

        val notificationHelper = NotificationHelper(requireContext())

        medicine.doses
            .filterValues { v ->
                v.getTimesAfterOrEqual(LocalDate.now())
                    .any { localDateTime -> localDateTime.toLocalDate().isEqual(LocalDate.now()) }
            }
            .forEach { (_, dose) ->
                val pair =
                    dose to notificationHelper.scheduleNotification(
                        "Kiss my Moby Dick",
                        "And while you're at it you may as well...",
                        dose.timeBegin.hour,
                        dose.timeBegin.minute,
                    )

                NotificationHelper.scheduledNotifications[dose.id] = pair
            }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param medicine Parameter 1.
         * @return A new instance of fragment MedicineEdit.
         */
        @JvmStatic
        fun newInstance(medicine: Medicine? = null) =
            MedicineEditFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(ARG_MEDICINE, medicine)
                }
            }
    }
}