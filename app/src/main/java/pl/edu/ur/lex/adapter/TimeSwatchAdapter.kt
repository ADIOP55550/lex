package pl.edu.ur.lex.adapter

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import pl.edu.ur.lex.R
import pl.edu.ur.lex.SettingsActivity
import kotlin.streams.toList

class TimeSwatchAdapter(
    private val context: SettingsActivity,
    private val timeSwatchList: HashMap<String, Int>,
    private val preferences: SharedPreferences,

    ) : RecyclerView.Adapter<TimeSwatchAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_timeswatch, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return timeSwatchList.size
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val timeSwatch = timeSwatchList.entries.stream().toList()

        holder.timeSwatchName.text = timeSwatch[position].key
        val hours = timeSwatch[position].value / 100
        val minutes = timeSwatch[position].value - hours * 100

        holder.timeSwatchHours.text = hours.toString()

        if (minutes < 10) {
            holder.timeSwatchMinutes.text = "0" + minutes.toString()
        } else {
            holder.timeSwatchMinutes.text = minutes.toString()
        }

        if (hours < 10) {
            holder.timeSwatchHours.text = "0" + hours.toString()
        } else {
            holder.timeSwatchHours.text = hours.toString()
        }

        holder.delButton.setOnClickListener {
            preferences.edit().remove(timeSwatch[position].key).apply()
            timeSwatchList.remove(timeSwatch[position].key)
            notifyDataSetChanged()
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val timeSwatchName: TextView = view.findViewById(R.id.timeSwatchName)
        val timeSwatchHours: TextView = view.findViewById(R.id.timeSwatchHours)
        val timeSwatchMinutes: TextView = view.findViewById(R.id.timeSwatchMinutes)
        val delButton: Button = view.findViewById(R.id.delButton)
    }
}
