package pl.edu.ur.lex

import android.content.SharedPreferences
import pl.edu.ur.lex.data.model.Dose
import java.lang.reflect.Field
import java.time.LocalDate
import java.time.LocalTime

object ConfigurationManager {

    // here custom config elements shall be added

    private val timeSwatchesDefault: Map<String, LocalTime> = mapOf(
        "Morning" to LocalTime.of(7, 0),
        "Noon" to LocalTime.of(12, 0),
        "Evening" to LocalTime.of(16, 0),
        "Night" to LocalTime.of(20, 0),
    )

    var timeSwatches: MutableMap<String, LocalTime> = timeSwatchesDefault.toMutableMap()
        get() = field
        set(value) {
            field = value
        }

    private const val sosNumberDefault: String = "997"

    var sosNumber: String = sosNumberDefault + ""
        get() = field
        set(value) {
            field = value
        }


    fun resetValue(field: String) {
        val defaultField: Field
        try {
            defaultField = this.javaClass.getDeclaredField(field + "Default")
        } catch (e: NoSuchFieldException) {
            throw NoSuchFieldException("Field $field has no default value field declared in ${this.javaClass.simpleName} (searched for ${field}Default)").initCause(
                e)
        }
        val declaredField = this.javaClass.getDeclaredField(field)

        if (declaredField.type != defaultField.type)
            throw TypeCastException("Default and target fields of different type")

        val defaultValue = defaultField.get(this)
        declaredField.set(this, defaultValue)
    }

    fun loadTimeSwatchesFromPreferences(preferences: SharedPreferences) {
        resetValue("timeSwatches")

        preferences.all.forEach {
            val hours = it.value as Int / 100
            val minute = it.value as Int - hours * 100
            timeSwatches[it.key] = LocalTime.of(hours, minute)
        }
        println(timeSwatches.entries)
    }

}