package pl.edu.ur.lex.exceptions

class ConflictException(s: String) : Throwable(s)
