package pl.edu.ur.lex

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.*
import com.google.firebase.database.R
import com.google.firebase.ktx.Firebase
import pl.edu.ur.lex.adapter.TimeSwatchAdapter
import pl.edu.ur.lex.adapter.UserAdapter
import pl.edu.ur.lex.data.model.User
import pl.edu.ur.lex.databinding.FragmentUsersBinding
import java.util.ArrayList


class UsersFragment : Fragment() {

    private lateinit var binding: FragmentUsersBinding
    var userList = ArrayList<User>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {

        binding = FragmentUsersBinding.inflate(inflater, container, false)


        binding.userRecyclerViewActivity.layoutManager =
            LinearLayoutManager(this.context, RecyclerView.VERTICAL, false)

        getUsersList()

        return binding.root
    }

    private fun getUsersList() {
        val firebase: FirebaseUser = FirebaseAuth.getInstance().currentUser!!
        val databaseReference: DatabaseReference =
            FirebaseDatabase.getInstance().getReference("users")

        databaseReference.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError) {
                Toast.makeText(context, error.message, Toast.LENGTH_SHORT).show()
            }

            override fun onDataChange(snapshot: DataSnapshot) {
                userList.clear()

                snapshot.children.forEach {
                    val user = it.getValue(User::class.java)
                    if (user!!.userId != firebase.uid) {
                        userList.add(user)
                    }
                }

                val userAdapter = UserAdapter(this@UsersFragment, userList)

                binding.userRecyclerViewActivity.adapter = userAdapter
            }

        })
    }

    override fun onResume() {
        super.onResume()
        getUsersList()
    }

    override fun onPause() {
        super.onPause()
        getUsersList()
    }
}

