package pl.edu.ur.lex.data.model

data class User(
    var userId: String = "",
    var userName: String = "",
    var email: String = "",
    val profileImage: String? = "",
)

