package pl.edu.ur.lex

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.auth.FirebaseAuth
import pl.edu.ur.lex.databinding.ActivityMainBinding
import pl.edu.ur.lex.firebase.DatabaseManager
import pl.edu.ur.lex.firebase.DatabaseManager.auth
import pl.edu.ur.lex.medicines.MedicineEditActivity
import java.time.LocalTime
import java.time.format.DateTimeFormatter


class MainActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityMainBinding

    private var loginMenuItem: MenuItem? = null


    lateinit var bottomNav: BottomNavigationView

    private fun gotoLogin() {
        startActivity(Intent(this, LoginActivity::class.java))
    }

    private fun gotoLoginAndBack() {
        startActivity(Intent(this, LoginActivity::class.java)
            .putExtra("backTo", MainActivity::class.java)
        )
        finish()
    }

    private fun gotoRegistration() {
        startActivity(Intent(this, RegistrationActivity::class.java))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (auth.currentUser == null) {
            gotoLoginAndBack()
            finish()
        }

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)

        loadFragment(FirstFragment())

        bottomNav = findViewById<BottomNavigationView>(R.id.bottomNav)
        bottomNav.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.home -> {
                    loadFragment(FirstFragment())
                    true
                }
                R.id.message -> {
                    loadFragment(UsersFragment())
                    true
                }
//                R.id.settings -> {
//                    loadFragment(SettingFragment())
//                    true
//                }
                else -> {
                    true
                }

            }
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        updateMenuLoginLogoutButton(DatabaseManager.auth, menu!!)

        return super.onPrepareOptionsMenu(menu)
    }

    override fun onResume() {
        super.onResume()
        val userid = auth.uid.toString()
        val preferences: SharedPreferences =
            getSharedPreferences("CustomTimeSwatches$userid",
                MODE_PRIVATE)

        loadTimeSwatches(preferences)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        updateMenuLoginLogoutButton(DatabaseManager.auth, menu)

        auth.addAuthStateListener {
            updateMenuLoginLogoutButton(it, menu)
            if (it.currentUser == null) {
                gotoLoginAndBack()
            }
        }

        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    private fun updateMenuLoginLogoutButton(
        auth: FirebaseAuth,
        menu: Menu,
    ) {
        if (loginMenuItem == null) {
            val el = menu.add(".")
            loginMenuItem = el
            el.setOnMenuItemClickListener {
                if (auth.currentUser != null) {
                    logoutUser(it)
                    true
                } else {
                    gotoLogin()
                    true
                }
            }
        }

        if (auth.currentUser != null)
            loginMenuItem!!.title = "Logout"
        else
            loginMenuItem!!.title = "Login"
    }

    private fun removeLoginLogoutButton(menu: Menu) {
        if (loginMenuItem != null)
            menu.removeItem(loginMenuItem!!.itemId)

        loginMenuItem = null
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> startActivity(Intent(this@MainActivity,
                SettingsActivity::class.java)).let { true }
            R.id.action_register -> gotoRegistration().let { true }
            R.id.action_Sos -> startActivity(Intent(this@MainActivity,
                SosNumberActivity::class.java)).let { true }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_content_main)
        return navController.navigateUp(appBarConfiguration)
                || super.onSupportNavigateUp()
    }

    private fun logoutUser(item: MenuItem) {
        auth.signOut()
    }

    fun onClickAddMedicine(view: View) {
        startActivity(Intent(this, MedicineEditActivity::class.java))

    }

    fun onClickPortraitOption2(view: View) {}
    fun onClickPortraitOption3(view: View) {}
    fun onClickPortraitOption4(view: View) {}


    private fun loadFragment(fragment: Fragment) {
        supportFragmentManager.commit {
            setCustomAnimations(
                R.anim.fade_in,
                R.anim.fade_out,
            )
            replace(R.id.nav_host_fragment_content_main, fragment)
        }
    }

    private fun loadTimeSwatches(preferences: SharedPreferences) {

        ConfigurationManager.loadTimeSwatchesFromPreferences(preferences)
    }
}
