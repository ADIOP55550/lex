package pl.edu.ur.lex.firebase

import android.security.keystore.UserNotAuthenticatedException
import android.util.Log
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DatabaseReference
import pl.edu.ur.lex.data.model.Medicine
import java.util.*
import java.util.function.Function

val MedicineManager = MedicineManagerReal

@Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
object MedicineManagerReal : Repository<Medicine, String, String>() {
    private val database = DatabaseManager.database

    private fun <T> forCurrentUser(action: Function<FirebaseUser, T>): T {
        DatabaseManager.auth.currentUser.let { user ->
            if (user == null) {
                val err =
                    UserNotAuthenticatedException("Cannot access medicines of not logged in user")
                @Suppress("UNCHECKED_CAST")
                return Tasks.forException<UserNotAuthenticatedException>(err) as T
            }

            return action.apply(user)
        }

    }

    override fun findAll(): Task<Collection<Medicine>> {

        return forCurrentUser { user ->
            val medicinesPathForUser = DatabaseManager.medicinesPathForUser(user.uid)
            Log.d("log", medicinesPathForUser)
            database.getReference(medicinesPathForUser).get()
                .onSuccessTask { snapshot ->
                    Tasks.forResult(
                        snapshot.children
                            .mapNotNull { data ->
                                val medicine: Medicine?

                                try {
                                    println(data.child("doses/0/timeBegin"))
                                    println(data)
                                    println(data.value)
                                    medicine = data.getValue(Medicine::class.java)
                                } catch (e: Exception) {
                                    Log.e("ERROR GETTING MEDICINE", e.toString())
                                    e.printStackTrace()
                                    throw e
                                }

                                Log.d("got medicine", medicine!!.toString())
                                medicine
                            }
                            .map { mapMedicineFromDatabase(it) }
                    )
                }
        }

    }

    private fun mapMedicineFromDatabase(medicine: Medicine): Medicine {
        medicine.doses =
            medicine.doses.values
                .map { dose ->
                    DoseManagerReal.mapToRealDose(dose,
                        medicine)
                }
                .associateBy { v -> v.id }
                .toMutableMap()

        return medicine
    }

    override fun findOne(id: String): Task<Optional<Medicine>> {
        return forCurrentUser { user ->
            database.getReference(DatabaseManager.medicinePathForUser(user.uid,
                id)).get()
                .onSuccessTask { snapshot ->
                    Tasks.forResult(Optional.ofNullable(snapshot.getValue(Medicine::class.java))
                        .map { mapMedicineFromDatabase(it) })
                }
        }
    }

    override fun findMany(name: String): Task<Collection<Medicine>> {
        return forCurrentUser { user ->
            database.getReference(DatabaseManager.medicinesPathForUser(user.uid)).get()
                .onSuccessTask { snapshot ->
                    Tasks.forResult(snapshot.children
                        .mapNotNull { data ->
                            data.getValue(Medicine::class.java)
                        }
                        .filter { it.name.contains(name) }
                        .map { mapMedicineFromDatabase(it) })
                }
        }
    }

    override fun create(value: Medicine): Task<Medicine> {
        println("Creating medicine")
        println(value)

        val ref = getNewMedicineRef()
        value.id = ref.key.toString()

        val dm = DoseManager.forMedicine(value)

        return Tasks.whenAllComplete(value.doses.values.map(dm::create)).continueWithTask {
            ref.setValue(value)
            Tasks.forResult(value)
        }
            .addOnFailureListener {
                it.printStackTrace()
                throw it
            }
    }

    override fun update(value: Medicine): Task<Medicine> {
        return forCurrentUser { user ->
            val dm = DoseManager.forMedicine(value)
            value.doses.forEach { dm.createOrUpdate(it.value) }

            val ref = database.getReference(DatabaseManager.medicinePathForUser(user.uid, value.id))
            ref.setValue(value).onSuccessTask {
                Tasks.forResult(value)
            }
        }
    }

    override fun createOrUpdate(value: Medicine): Task<Medicine> {
        return if (value.id.isNotEmpty())
            update(value)
        else
            create(value)
    }

    override fun delete(value: Medicine): Task<Void> {
        return forCurrentUser {
            val ref = database.getReference(DatabaseManager.medicinePathForUser(it.uid, value.id))
            ref.removeValue()
        }
    }

    private fun getNewMedicineRef(): DatabaseReference {
        return forCurrentUser {
            database.getReference(DatabaseManager.medicinesPathForUser(it.uid)).push()
        }
    }
}
