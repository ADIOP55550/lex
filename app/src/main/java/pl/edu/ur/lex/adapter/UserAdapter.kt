package pl.edu.ur.lex.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.bumptech.glide.Glide
import pl.edu.ur.lex.ChatActivity
import pl.edu.ur.lex.R
import pl.edu.ur.lex.UsersFragment
import pl.edu.ur.lex.data.model.User
import java.net.URL


class UserAdapter(private val context: UsersFragment, private val userList: ArrayList<User>) :
    RecyclerView.Adapter<UserAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_user, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = userList[position]
        holder.txtUserName.text = user.userName
        holder.imgUser.load(user.profileImage)

        holder.layoutUser.setOnClickListener {
            val intent = Intent(this.context.context, ChatActivity::class.java)
            intent.putExtra("userId", user.userId)
            intent.putExtra("userImage", user.profileImage)
            intent.putExtra("userName", user.userName)
            context.startActivity(intent)
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val txtUserName: TextView = view.findViewById(R.id.userName)
        val imgUser: ImageView = view.findViewById(R.id.userImage)
        val layoutUser: LinearLayout = view.findViewById(R.id.layoutUser)
    }


}