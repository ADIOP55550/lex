package pl.edu.ur.lex.exceptions

class EmptyInputException(s: String) : Throwable(s)
