package pl.edu.ur.lex

import android.annotation.SuppressLint
import android.content.ContentValues.TAG
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.auth.FirebaseAuthWeakPasswordException
import com.google.firebase.auth.ktx.userProfileChangeRequest
import pl.edu.ur.lex.data.model.User
import pl.edu.ur.lex.firebase.DatabaseManager
import java.util.*
import kotlin.random.asKotlinRandom

val names: List<String> = listOf(
    "Russo",
    "Craig",
    "Glass",
    "Owens",
    "Anthony",
    "Wood",
    "Melton",
    "Jensen",
    "Carter",
    "Roberson",
    "Mueller",
    "Lam",
    "Bond",
    "Stephens",
    "Mann",
    "Ho",
    "Jordan",
    "Farrell",
    "Romero",
    "Aguirre",
    "Harrell",
    "Stewart",
    "Carpenter",
    "Bartlett",
    "Powers",
    "Vincent",
    "Tyler",
    "Saunders",
    "Levine",
    "Gates",
    "Woodward",
    "Fitzgerald",
    "Michael",
    "Rich",
    "Johnston",
    "Goodwin",
    "Ponce",
    "Robles",
    "Keller",
    "Callahan",
    "Hartman",
    "Pham",
    "Love",
    "Best",
    "Coffey",
    "Bright",
    "Clarke",
    "Strickland",
    "Hood",
)
val lastNames: List<String> = listOf(
    "Lyla",
    "Abdiel",
    "Jamarcus",
    "Carina",
    "Jakobe",
    "Kamari",
    "Louis",
    "Janiah",
    "Dominique",
    "Kelvin",
    "Lila",
    "Austin",
    "Aditya",
    "Adam",
    "Arturo",
    "Mallory",
    "Marvin",
    "Rose",
    "Kallie",
    "Jaydon",
    "Lorena",
    "Amya",
    "Jordyn",
    "Konnor",
    "Essence",
    "Sloane",
    "Soren",
    "Stanley",
    "Jocelynn",
    "Marcus",
    "Rowan",
    "Wade",
    "Perla",
    "Izaiah",
    "Finn",
    "Layla",
    "Angelina",
    "Catalina",
    "Finnegan",
    "Davis",
    "Dwayne",
    "Charlee",
    "Rory",
    "Jax",
    "Mikaela",
    "Iris",
    "Kaylin",
    "Princess",
    "Selena",
    "Hadassah",
)
val domains = listOf("example", "abc", "email", "ab")
val tlds = listOf("pl", "com", "edu.pl", "org")

class RegistrationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

        findViewById<TextView>(R.id.login_link).setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }

    fun registerUser(view: View) {
        val emailEdit = findViewById<EditText>(R.id.email_edit_text)
        val nameEdit = findViewById<EditText>(R.id.name_edit_text)
        val passwordEdit = findViewById<EditText>(R.id.password_edit_text)
        val passwordRepeatEdit = findViewById<EditText>(R.id.re_password_edit_text)

        val email: String = emailEdit.text.toString()
        val name: String = nameEdit.text.toString()
        val password: String = passwordEdit.text.toString()
        val passwordRepeat: String = passwordRepeatEdit.text.toString()

        var errors = false

        if (email.isBlank()) {
            errors = true
            Toast.makeText(this, "Email cannot be empty!", Toast.LENGTH_SHORT).show()
            emailEdit.error = "Email cannot be empty!"
        }

        if (name.isBlank()) {
            errors = true
            Toast.makeText(this, "Name cannot be empty!", Toast.LENGTH_SHORT).show()
            nameEdit.error = "Name cannot be empty!"
        }

        if (password.isBlank()) {
            errors = true
            Toast.makeText(this, "Password cannot be empty!", Toast.LENGTH_SHORT).show()
            passwordEdit.error = "Password cannot be empty!"
        }

        if (password != passwordRepeat) {
            errors = true
            Toast.makeText(this, "Passwords don't match", Toast.LENGTH_SHORT).show()
            passwordRepeatEdit.error = "Passwords don't match!"
        }

        if (errors) return

        DatabaseManager.auth.createUserWithEmailAndPassword(email, password)
            .addOnSuccessListener { authResult: AuthResult? ->

                Objects.requireNonNull(authResult)
                Objects.requireNonNull(authResult!!.user)

                val user = authResult.user!!

                val photoUriV = Uri.parse(
                    "https://api.dicebear.com/5.x/personas/png?seed="
                            + user.uid.substring(0, 2)
                )
                val profileUpdates = userProfileChangeRequest {
                    displayName = name
                    // This implementation is a remix of Personas by Draftbit by Draftbit - draftbit.com. Licensed under CC BY 4.0.
                    // https://draftbit.com/
                    // https://creativecommons.org/licenses/by/4.0/
                    // from https://dicebear.com/styles/personas
                    photoUri =
                        photoUriV
                }

                user.updateProfile(profileUpdates)
                    .addOnFailureListener {
                        Log.d(TAG, "User profile could not be updated.")
                    }
                    .addOnSuccessListener { _ ->
                        val u = User(user.uid,
                            name,
                            user.email.toString(),
                            photoUriV.toString())

                        DatabaseManager.database.getReference("users")
                            .child(user.uid)
                            .setValue(u)
                            .addOnSuccessListener {
                                Toast.makeText(this, "Registration Successful", Toast.LENGTH_SHORT)
                                    .show()
                                startActivity(Intent(this, MainActivity::class.java))
                                finish()
                            }
                            .addOnFailureListener {
                                Toast.makeText(this, "Failed to save user", Toast.LENGTH_SHORT)
                                    .show()
                            }
                    }


            }
            .addOnFailureListener {
                when (it) {
                    is FirebaseAuthWeakPasswordException -> {
                        passwordEdit.error = it.reason
                        Toast.makeText(this, it.localizedMessage, Toast.LENGTH_SHORT)
                            .show()
                    }
                    is FirebaseAuthUserCollisionException -> {
                        emailEdit.error = it.localizedMessage
                        Toast.makeText(this, it.localizedMessage, Toast.LENGTH_SHORT)
                            .show()
                    }
                    else -> {
                        Toast.makeText(this,
                            "An error occurred: ${it.localizedMessage}",
                            Toast.LENGTH_LONG)
                            .show()
                    }
                }
            }
            .addOnCanceledListener {
                Toast.makeText(this, "Registration Cancelled", Toast.LENGTH_SHORT).show()
            }
    }

    @SuppressLint("SetTextI18n")
    fun randomData(view: View) {
        val emailEdit = findViewById<EditText>(R.id.email_edit_text)
        val nameEdit = findViewById<EditText>(R.id.name_edit_text)
        val passwordEdit = findViewById<EditText>(R.id.password_edit_text)
        val passwordRepeatEdit = findViewById<EditText>(R.id.re_password_edit_text)

        val name = names.random()
        val lastName = names.random()
        val email = name[0].lowercaseChar()
            .toString() + lastName.lowercase() + "@" + domains.random() + "." + tlds.random()

        val r = Random()
        // password is a random substring of length 6 from this string
        val h =
            "6e1ebd05934a0e6e7c2f862a69da3c6189c53f268348964a4c134e322c20186a821732893e746de13866fdde67c6a103ef13b44d6b322b1fb2101786f79abfa7"

        val startIndex = r.asKotlinRandom().nextInt(0, h.length - 6)
        val password = h.substring(startIndex, startIndex + 6)


        emailEdit.setText(email)
        nameEdit.setText("$name $lastName")
        passwordEdit.setText(password)
        passwordRepeatEdit.setText(password)

    }
}
