package pl.edu.ur.lex

import android.app.*
import android.content.ContentResolver
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Context.USAGE_STATS_SERVICE
import android.content.Intent
import android.media.AudioAttributes
import android.net.Uri
import android.provider.MediaStore.Audio
import java.util.*

var notificationID = 0
const val channelID = "channel1"

class NotificationHelper(val context: Context) {

    init {
        createNotificationChannel()
    }

    fun scheduleNotification(title: String, message: String, hour: Int, minute: Int): Int {
        val intent = Intent(context, NotificationReciver::class.java)
        intent.putExtra("titleExtra", title)
        intent.putExtra("messageExtra", message)

        val pendingIntent = PendingIntent.getBroadcast(
            context,
            ++notificationID,
            intent,
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
        )

        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager

        val time = getTime(hour, minute)

        alarmManager.setExactAndAllowWhileIdle(
            AlarmManager.RTC_WAKEUP,
            time,
            pendingIntent
        )
        showAlert(time, title, message)

        return notificationID
    }

    private fun showAlert(time: Long, title: String, message: String) {
        val date = Date(time)
        val dateFormat = android.text.format.DateFormat.getLongDateFormat(context)
        val timeFormat = android.text.format.DateFormat.getTimeFormat(context)

        AlertDialog.Builder(context)
            .setTitle("Notification Scheduled")
            .setMessage(
                "Title: " + title +
                        "\nMessage: " + message +
                        "\nAt: " + dateFormat.format(date) + " " + timeFormat.format(date))
            .setPositiveButton("Okay") { _, _ -> }
            .show()
    }

    private fun getTime(hour: Int, minute: Int): Long {

        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, hour)
        calendar.set(Calendar.MINUTE, minute)
        return calendar.timeInMillis
    }

    private fun createNotificationChannel() {
        val soundUri: Uri =
            Uri.parse("android.resource://"+context.packageName +"/"+R.raw.powiadomienie)
        println(soundUri)
        println(soundUri)
        println(soundUri)

        val name = "Notif Channel"
        val desc = "A Description of the Channel"
        val importance = NotificationManager.IMPORTANCE_DEFAULT
        val channel = NotificationChannel(channelID, name, importance)
        channel.description = desc
        val audioAttributes = AudioAttributes.Builder()
            .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
            .setUsage(AudioAttributes.USAGE_NOTIFICATION)
            .build()
        channel.setSound(soundUri, audioAttributes)
        val notificationManager =
            context.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
    }

    companion object {
        val scheduledNotifications: MutableMap<DoseId, Pair<Dose, Int>> = mutableMapOf()
    }
}
