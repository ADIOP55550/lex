package pl.edu.ur.lex

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import pl.edu.ur.lex.databinding.ActivityLoginBinding
import pl.edu.ur.lex.firebase.DatabaseManager

class LoginActivity : AppCompatActivity() {

    private var backTo: Class<*>? = null
    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        backTo = intent.getSerializableExtra("backTo") as Class<*>?
        if (backTo == null) backTo = MainActivity::class.java

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)


        findViewById<TextView>(R.id.register_link).setOnClickListener { goToRegister() }
    }

    private fun goToRegister() {
        startActivity(Intent(this, RegistrationActivity::class.java))
        finish()
    }

    private fun goBack() {
        startActivity(Intent(this, backTo))
        finish() // end the activity
    }

    fun loginUser(view: View) {

        val email: String = binding.loginEmailEditText.text.toString()
        val password: String = binding.loginPasswordEditText.text.toString()
        if (email.isEmpty()) {
            binding.loginEmailEditText.error = "Email cannot be empty"
            return
        }
        if (password.isEmpty()) {
            binding.loginPasswordEditText.error = "Password cannot be empty"
            return
        }


        DatabaseManager.auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Toast.makeText(this, "Login successful", Toast.LENGTH_SHORT).show()
                    goBack()
                } else {
                    Toast.makeText(this,
                        "Unable to login. Check your input or try again later",
                        Toast.LENGTH_SHORT).show()
                }
            }
    }

    @SuppressLint("SetTextI18n")
    fun fillGood(view: View) {
        binding.loginEmailEditText.setText("abc@example.com")
        binding.loginPasswordEditText.setText("abcDef")
    }

}