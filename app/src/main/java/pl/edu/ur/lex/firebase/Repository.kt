package pl.edu.ur.lex.firebase

import android.content.res.Resources
import com.google.android.gms.tasks.Task
import java.util.*

abstract class Repository<T, IdType, ManySearchType> {
    abstract fun findAll(): Task<Collection<T>>
    abstract fun findOne(id: IdType): Task<Optional<T>>
    fun findOneOrFail(id: IdType): Task<T> {
        return findOne(id).continueWithTask { task ->
            task.addOnSuccessListener { optional ->
                optional.orElseThrow { Resources.NotFoundException() }
            }
            task.continueWith {
                it.result.get()
            }
        }
    }

    abstract fun findMany(value: ManySearchType): Task<Collection<T>>
    fun findManyOrFail(value: ManySearchType): Task<Collection<T>> {
        return findMany(value).continueWithTask { task ->
            task.addOnSuccessListener { col ->
                if (col.isEmpty()) {
                    throw Resources.NotFoundException()
                }
            }
        }
    }

    abstract fun create(value: T): Task<T>
    abstract fun update(value: T): Task<T>
    abstract fun createOrUpdate(value: T): Task<T>
    abstract fun delete(value: T): Task<Void>
}

