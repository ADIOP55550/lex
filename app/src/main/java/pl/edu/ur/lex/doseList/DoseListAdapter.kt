package pl.edu.ur.lex.doseList

import android.graphics.Paint
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import pl.edu.ur.lex.data.model.Dose
import pl.edu.ur.lex.databinding.FragmentDoseBinding
import pl.edu.ur.lex.firebase.DoseManager
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*


/**
 * [RecyclerView.Adapter] that can display a [Dose].
 */
class DoseListAdapter(
    private val values: List<Dose>,
    var selectedDate: Date,
) : RecyclerView.Adapter<DoseListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(FragmentDoseBinding.inflate(LayoutInflater.from(parent.context),
            parent,
            false))

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.bind(item)
    }

    override fun onViewRecycled(holder: ViewHolder) {
        super.onViewRecycled(holder)
        holder.unbind()
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(binding: FragmentDoseBinding) : RecyclerView.ViewHolder(binding.root) {
        private var dose: Dose? = null

        private val takenCheckBox: CheckBox = binding.takenCheckBox
        private val timeLabel: TextView = binding.timeLabel
        private val medicineLabel: TextView = binding.medicineLabel
        private val idLabel: TextView = binding.idView

        private val listener: (buttonView: CompoundButton, isChecked: Boolean) -> Unit =
            { _, isChecked ->
                val dose = dose!!
                if (isChecked) {
                    if (!dose.taken.contains(selectedDate))
                        dose.taken.add(selectedDate)
                } else
                    dose.taken.remove(selectedDate)

                DoseManager.forMedicine(dose.medicine).update(dose)

                updateStrikeThrough(dose)
            }

        override fun toString(): String {
            return ""
        }

        fun bind(item: Dose) {
            dose = item
            val dose = dose!!
            takenCheckBox.isChecked = dose.taken.contains(selectedDate)
            timeLabel.text = dose.timeBegin.toLocalTime()
                .format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT))
            medicineLabel.text = dose.medicine.name
            idLabel.text = dose.id

            updateStrikeThrough(dose)
            takenCheckBox.setOnCheckedChangeListener(listener)
        }

        fun unbind() {
            takenCheckBox.setOnCheckedChangeListener(null)
            takenCheckBox.isChecked = false
        }

        private fun updateStrikeThrough(dose: Dose) {
            val labels = arrayOf(medicineLabel, timeLabel)

            if (dose.taken.contains(selectedDate)) {
                labels.forEach {
                    it.paintFlags = it.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                }
            } else {
                labels.forEach {
                    it.paintFlags = it.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
                }
            }
        }
    }

}