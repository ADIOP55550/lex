package pl.edu.ur.lex.data.model

data class Chat(
    var senderId: String = "",
    var receiverId: String = "",
    var message: String = "",
    val time: String? = "",
)