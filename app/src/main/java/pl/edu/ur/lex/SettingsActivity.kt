package pl.edu.ur.lex

import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TimePicker
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.ui.AppBarConfiguration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import pl.edu.ur.lex.adapter.TimeSwatchAdapter
import pl.edu.ur.lex.databinding.ActivitySettingsBinding
import pl.edu.ur.lex.firebase.DatabaseManager


class SettingsActivity : AppCompatActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivitySettingsBinding
    private lateinit var preferences: SharedPreferences
    private var customTimeSwatches = HashMap<String, Int>()
    var time: Int? = null

    @RequiresApi(Build.VERSION_CODES.S)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySettingsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)

        binding.timeSwatchesRecycler.layoutManager =
            LinearLayoutManager(this, RecyclerView.VERTICAL, false)

        val editText: EditText = findViewById<EditText>(R.id.etName)
        val addButton: Button = findViewById<Button>(R.id.addTimeSwatchesButton)
        val timePicker: TimePicker = findViewById<TimePicker>(R.id.timePicker)

        val userid = DatabaseManager.auth.uid.toString()
        val reservedTimeSwatchNames = listOf("Custom...")

        println(userid)

        preferences =
            getSharedPreferences("CustomTimeSwatches$userid",
                MODE_PRIVATE)
        val editData: SharedPreferences.Editor = preferences.edit()

        getAllTimeSwatches()

        addButton.setOnClickListener {
            if (preferences.contains(editText.text.toString())) {
                editText.error = "This name already exists"
                return@setOnClickListener
            }

            if (editText.text.isBlank()) {
                editText.error = "Empty"
                return@setOnClickListener
            }

            time = timePicker.hour * 100 + timePicker.minute

            if (reservedTimeSwatchNames.contains(editText.text.trim().toString())) {
                editText.error = "This name is reserved"
                return@setOnClickListener
            }

            editData.putInt(editText.text.toString(), time!!)
            editData.apply()


            ConfigurationManager.loadTimeSwatchesFromPreferences(preferences)
            time = null
            editText.setText("")

            getAllTimeSwatches()

            NotificationHelper(this).scheduleNotification("title", "titleagain", 15, 50)
        }

    }

    private fun getAllTimeSwatches() {
        customTimeSwatches.clear()

        preferences.all.forEach {
            customTimeSwatches[it.key] = it.value as Int
        }

        val timeSwatchesAdapter =
            TimeSwatchAdapter(this@SettingsActivity,
                customTimeSwatches,
                preferences)

        binding.timeSwatchesRecycler.adapter = timeSwatchesAdapter
    }
}
