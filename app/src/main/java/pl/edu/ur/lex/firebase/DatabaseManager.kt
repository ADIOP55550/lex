package pl.edu.ur.lex.firebase

import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

object DatabaseManager {
    val database by lazy { Firebase.database }
    val auth by lazy { Firebase.auth }

    const val medicinesPath = "medicines"
    fun medicinesPathForUser(uid: String): String = "$medicinesPath/$uid"
    fun medicinePathForUser(uid: String, id: String): String = "${medicinesPathForUser(uid)}/$id"

    fun dosesPathForUserAndMedicine(uid: String, medicine_id: String): String =
        "${medicinePathForUser(uid, medicine_id)}/doses"

    fun dosePathForUserAndMedicine(uid: String, medicine_id: String, id: String): String =
        "${dosesPathForUserAndMedicine(uid, medicine_id)}/$id"
}