package pl.edu.ur.lex

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import pl.edu.ur.lex.data.model.Dose
import pl.edu.ur.lex.databinding.DoseBinding

/**
 * [RecyclerView.Adapter] that can display a [Pair<String, Dose>].
 */
class DoseRecyclerViewAdapter(
    private val values: List<Pair<String, Dose>>,
    private val onDelete: (Pair<String, Dose>) -> Unit,
    private val onEditStart: (Pair<String, Dose>) -> Unit,
) : RecyclerView.Adapter<DoseRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(DoseBinding.inflate(LayoutInflater.from(parent.context), parent, false),
            onDelete, onEditStart)

    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.bind(item)
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(
        binding: DoseBinding,
        private val onDelete: (Pair<String, Dose>) -> Unit,
        private val onEditStart: (Pair<String, Dose>) -> Unit,
    ) :
        RecyclerView.ViewHolder(binding.root) {
        val contentLabel: TextView = binding.contentLabel
        val timeLabel: TextView = binding.timeLabel

        //        val removeBtn: Button = binding.removeButton
        private var currentDosePair: Pair<String, Dose>? = null

        init {
            binding.removeButton.setOnClickListener {
                currentDosePair?.let { onDelete(it) }
            }

            binding.root.setOnClickListener {
                currentDosePair?.let { onEditStart(it) }
            }

        }

        override fun toString(): String {
            return super.toString() + " '" + timeLabel.text + " " + contentLabel.text + "'"
        }

        fun bind(pair: Pair<String, Dose>) {
            currentDosePair = pair

            timeLabel.text = pair.second.timeBegin.toLocalTime().toString()
            contentLabel.text = pair.second.getRepetitionAsString()
        }

    }

}