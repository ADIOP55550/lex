package pl.edu.ur.lex.medicines

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.ToggleButton
import androidx.core.view.children
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import pl.edu.ur.lex.ConfigurationManager
import pl.edu.ur.lex.R
import pl.edu.ur.lex.data.model.*
import pl.edu.ur.lex.databinding.FragmentDoseEditBinding
import java.text.DateFormat
import java.time.DayOfWeek
import java.time.LocalTime
import java.time.ZonedDateTime
import java.util.*

private const val ARG_MEDICINE = "medicine"
private const val ARG_DOSE = "dose"

/**
 * A simple [Fragment] subclass.
 * Use the [DoseEditFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class DoseEditFragment : Fragment() {
    private var skipNextTimeSelection: Boolean = false
    private var timeSwatchSpinnerArrayAdapter: ArrayAdapter<String>? = null
    private var indexOfCustom: Int = -1
    private var timeSwatchList: MutableList<MutableMap.MutableEntry<String, LocalTime>>? = null
    private var repetitionPeriods: List<RepetitionPeriod>? = null
    private var binding: FragmentDoseEditBinding? = null

    private lateinit var medicine: Medicine
    private var dose: Dose? = null

    private var initialized: Boolean = false

    private var selectedTime: LocalTime? = null
    private var selectedEndDate: Date? = null
    private var selectedPeriod: RepetitionPeriod? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { args ->
            medicine = args.getSerializable(ARG_MEDICINE) as Medicine
            dose = args.getSerializable(ARG_DOSE) as Dose?


            if (dose == null)
                dose = Dose(repeats = true)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        findNavController().previousBackStackEntry?.savedStateHandle?.remove<Any>("dose")

        binding = FragmentDoseEditBinding.inflate(inflater, container, false)
        val view = binding!!.root

        Objects.requireNonNull(binding)
        val binding = binding!!

        binding.datePicker.minDate = ZonedDateTime.now().minusMonths(1).toEpochSecond() * 1000

        loadDose()

        return view
    }

    private fun loadDose() {
        Objects.requireNonNull(binding)
        val binding = binding!!

        prepareControls(binding)

        if (dose != null) {
            loadDoseData(binding)
        }

        initialized = true
    }

    private fun loadDoseData(
        binding: FragmentDoseEditBinding,
    ) {
        val dose = dose!!

        val dateBegin = dose.dateBegin
        @Suppress("DEPRECATION")
        binding.datePicker.updateDate(dateBegin.year + 1900, dateBegin.month, dateBegin.date)

        val index =
            (timeSwatchList!!.indexOfFirst { v -> v.value == dose.timeBegin.toLocalTime() }).let {
                if (it == -1) {
                    timeSwatchList!![indexOfCustom].setValue(dose.timeBegin.toLocalTime())
                    recreateTimeSwatchSpinnerAdapter()

                    indexOfCustom
                } else it
            }

        skipNextTimeSelection = true
        binding.spinnerTimeSwatch.setSelection(index)



        binding.repetitionDetails.clearCheck()
        println(dose.repetitionStrategy)
        println(dose.getData())
        when (dose.repetitionStrategy) {
            RepetitionStrategy.EVERYDAY -> {
                binding.everydayRadioButton.isChecked = true
            }
            RepetitionStrategy.WEEKDAYS -> {
                binding.selectedDaysRadioButton.isChecked = true
                ((dose.getData() as RepetitionStrategyData.WEEKDAYS?)?.value)?.let {
                    if (it.contains(DayOfWeek.MONDAY)) binding.monToggleButton.isChecked = true
                    if (it.contains(DayOfWeek.TUESDAY)) binding.tueToggleButton.isChecked = true
                    if (it.contains(DayOfWeek.WEDNESDAY)) binding.wedToggleButton.isChecked =
                        true
                    if (it.contains(DayOfWeek.THURSDAY)) binding.thuToggleButton.isChecked =
                        true
                    if (it.contains(DayOfWeek.FRIDAY)) binding.friToggleButton.isChecked = true
                    if (it.contains(DayOfWeek.SATURDAY)) binding.satToggleButton.isChecked =
                        true
                    if (it.contains(DayOfWeek.SUNDAY)) binding.sunToggleButton.isChecked = true
                }
            }
            RepetitionStrategy.EVERY_N_PERIODS -> {
                binding.everyRadioButton.isChecked = true
                val p = (dose.getData() as RepetitionStrategyData.EVERY_N_PERIODS?)?.value
                if (p != null) {
                    binding.editTextNumberRepetitionPeriods.setText(p.first.toString())
                    binding.spinnerRepetitionPeriod.setSelection(repetitionPeriods!!.indexOf(p.second))
                }
            }
        }
        binding.repetitionDetails2.clearCheck()
        when (dose.repetitionEndStrategy) {
            RepetitionEndStrategy.INFINITE -> {
                binding.infiniteRadioButton.isChecked = true
            }
            RepetitionEndStrategy.N_TIMES -> {
                binding.xtimesRadioButton.isChecked = true
                val n = (dose.getEndData() as RepetitionEndStrategyData.N_TIMES?)?.value
                if (n != null)
                    binding.editTextNumberRepetitionXTimes.setText(n.toString())
            }
            RepetitionEndStrategy.UNTIL_DATE -> {
                binding.untilRadioButton.isChecked = true
                selectedEndDate =
                    (dose.getEndData() as RepetitionEndStrategyData.UNTIL_DATE?)?.value
                if (selectedEndDate != null)
                    binding.untilRadioButton.text =
                        "Until " + DateFormat.getDateInstance().format(selectedEndDate!!)
            }
        }

        binding.repeatingSwitch.isChecked = dose.repeats
    }

    private fun prepareControls(binding: FragmentDoseEditBinding) {
        val timeSwatchSpinner: Spinner = binding.spinnerTimeSwatch
        val customKey = "Custom..."

        timeSwatchList =
            ConfigurationManager.timeSwatches.let { m ->
                val copy = m.toMutableMap()
                copy[customKey] = LocalTime.NOON
                copy
            }.entries.toMutableList()

        val timeSwatchList = timeSwatchList!!
        this.indexOfCustom = timeSwatchList.indexOfLast { entry -> entry.key == customKey }

        timeSwatchSpinnerArrayAdapter = ArrayAdapter(
            this.requireContext(),
            android.R.layout.simple_spinner_item,
            timeSwatchList
                .map { entry -> entry.key + " (" + entry.value.toString() + ")" }
                .toMutableList()
        )

        timeSwatchSpinnerArrayAdapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        timeSwatchSpinner.adapter = timeSwatchSpinnerArrayAdapter

        timeSwatchSpinner.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long,
            ) {
                if (skipNextTimeSelection) {
                    skipNextTimeSelection = false
                    return
                }

                val previousSelection = timeSwatchSpinner.selectedItemPosition
                if (selectedTime == null) selectedTime = LocalTime.NOON

                if (position != indexOfCustom) {
                    selectedTime = timeSwatchList[position].value
                    return
                }

                val timePickerDialog = TimePickerDialog(
                    requireContext(),
                    { _, hourOfDay, minute ->
                        timeSwatchList[indexOfCustom].setValue(LocalTime.of(hourOfDay,
                            minute))
                        selectedTime = timeSwatchList[indexOfCustom].value

                        // We replace whole adapter because elements are mapped and
                        // Some smart people thought that it would be great to give us
                        // method .remove(obj: String) but not .remove(pos: Int)

                        recreateTimeSwatchSpinnerAdapter()

                        skipNextTimeSelection = true
                        timeSwatchSpinner.setSelection(indexOfCustom)
                    },
                    selectedTime!!.hour,
                    selectedTime!!.minute,
                    true
                )

                timePickerDialog.setOnCancelListener {
                    timeSwatchSpinner.setSelection(previousSelection)
                }

                timePickerDialog.show()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                selectedTime = null
            }
        }

        binding.medicineName.text = medicine.name
        binding.medicineDescription.text = medicine.description

        binding.addButton.text = if (dose?.id?.isNotEmpty() == true) "SAVE" else "ADD DOSE +"

        binding.addButton.setOnClickListener {
            saveDose()
        }

        binding.everyRadioButton.setOnCheckedChangeListener { _, isChecked ->
            setEnabledAllRepetitionEveryFields(isChecked)
        }

        binding.selectedDaysRadioButton.setOnCheckedChangeListener { _, isChecked ->
            setEnabledAllRepetitionWeekButtons(isChecked)
        }

        binding.repeatingSwitch.setOnCheckedChangeListener { _, isChecked ->
            binding.repetitionDetails.isVisible = isChecked
            binding.repetitionDetails2.isVisible = isChecked
        }

        binding.untilRadioButton.setOnCheckedChangeListener { _, isChecked ->
            if (!initialized)
                return@setOnCheckedChangeListener

            val date = Date()
            @Suppress("DEPRECATION")
            if (isChecked)
                DatePickerDialog(this.requireContext(), { _, year, month, dayOfMonth ->
                    print(year)
                    print("/")
                    print(month)
                    print("/")
                    println(dayOfMonth)

                    selectedEndDate = Date(year - 1900, month, dayOfMonth)
                    println(selectedEndDate)
                    Objects.requireNonNull(selectedEndDate)

                    binding.untilRadioButton.text =
                        "Until " + DateFormat.getDateInstance().format(selectedEndDate!!)
                }, date.year + 1900, date.month, date.date).show()
        }
        binding.xtimesRadioButton.setOnCheckedChangeListener { _, isChecked ->
            binding.editTextNumberRepetitionXTimes.isEnabled = isChecked
        }


        repetitionPeriods = RepetitionPeriod.values().toList()
        val repetitionPeriods = repetitionPeriods!!

        val adapter2 = ArrayAdapter(this.requireContext(),
            android.R.layout.simple_spinner_item,
            repetitionPeriods.map { v -> v.description }.toMutableList())
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        binding.spinnerRepetitionPeriod.adapter = adapter2

        binding.spinnerRepetitionPeriod.onItemSelectedListener = object : OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long,
            ) {
                selectedPeriod = repetitionPeriods[position]
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                selectedPeriod = null
            }
        }
    }

    private fun recreateTimeSwatchSpinnerAdapter() {
        timeSwatchSpinnerArrayAdapter = ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_item,
            timeSwatchList!!
                .map { entry -> entry.key + " (" + entry.value.toString() + ")" }
                .toMutableList()
        )

        timeSwatchSpinnerArrayAdapter!!.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding!!.spinnerTimeSwatch.adapter = timeSwatchSpinnerArrayAdapter
    }

    private fun setEnabledAllRepetitionWeekButtons(enabled: Boolean) {
        Objects.requireNonNull(binding)
        val binding = binding!!

        binding.weekToggleButtonGroup.children.iterator().forEach { v ->
            val tb = v as ToggleButton
            tb.isEnabled = enabled
        }
    }

    private fun setEnabledAllRepetitionEveryFields(enabled: Boolean) {
        Objects.requireNonNull(binding)
        val binding = binding!!

        binding.everyNPeriods.children.iterator().forEach {
            it.isEnabled = enabled
        }
    }

    private fun saveDose() {
        val binding = binding!!
        Objects.requireNonNull(dose)
        val dose = dose!!

        dose.dateBegin =
            binding.datePicker.let { Date(it.year - 1900, it.month, it.dayOfMonth) }

        if (selectedTime == null) {
            showError("No time selected!")
            return
        }

        dose.timeBegin = Time.fromLocalTime(selectedTime!!)

        dose.repeats = binding.repeatingSwitch.isChecked

        if (dose.repeats) {
            if (binding.repetitionDetails.checkedRadioButtonId == -1) {
                showError("No repetition strategy selected!")
                return
            }
            dose.repetitionStrategy = when (binding.repetitionDetails.checkedRadioButtonId) {
                R.id.everydayRadioButton -> RepetitionStrategy.EVERYDAY
                R.id.selectedDaysRadioButton -> RepetitionStrategy.WEEKDAYS
                R.id.everyRadioButton -> RepetitionStrategy.EVERY_N_PERIODS
                else -> throw Exception("Unknown repetitionStrategy radio id: " + binding.repetitionDetails.checkedRadioButtonId)
            }

            val repetitionStrategyData = when (binding.repetitionDetails.checkedRadioButtonId) {
                R.id.everydayRadioButton -> RepetitionStrategyData.EVERYDAY()

                R.id.selectedDaysRadioButton -> {
                    val a = mutableListOf<DayOfWeek>()
                    if (binding.monToggleButton.isChecked) a.add(DayOfWeek.MONDAY)
                    if (binding.tueToggleButton.isChecked) a.add(DayOfWeek.TUESDAY)
                    if (binding.wedToggleButton.isChecked) a.add(DayOfWeek.WEDNESDAY)
                    if (binding.thuToggleButton.isChecked) a.add(DayOfWeek.THURSDAY)
                    if (binding.friToggleButton.isChecked) a.add(DayOfWeek.FRIDAY)
                    if (binding.satToggleButton.isChecked) a.add(DayOfWeek.SATURDAY)
                    if (binding.sunToggleButton.isChecked) a.add(DayOfWeek.SUNDAY)
                    if (a.size == 0) {
                        showError("Select at least one day of the week")
                        return
                    }
                    RepetitionStrategyData.WEEKDAYS(a)
                }

                R.id.everyRadioButton -> {

                    val text = binding.editTextNumberRepetitionPeriods.text
                    if (text.isNullOrEmpty()) {
                        binding.editTextNumberRepetitionPeriods.error =
                            "Enter a number larger than 0"
                        return
                    }

                    val n = Integer.parseInt(text.toString())
                    if (n < 1) {
                        binding.editTextNumberRepetitionPeriods.error =
                            "Enter a number larger than 0"
                        return
                    }

                    if (selectedPeriod == null) {
                        showError("No repetition period selected!")
                        return
                    }

                    RepetitionStrategyData.EVERY_N_PERIODS(n to selectedPeriod!!)
                }

                else -> throw Exception("Unknown repetitionStrategy radio id: " + binding.repetitionDetails.checkedRadioButtonId)
            }

            dose.setData(repetitionStrategyData)

            dose.repetitionEndStrategy = when (binding.repetitionDetails2.checkedRadioButtonId) {
                R.id.infiniteRadioButton -> RepetitionEndStrategy.INFINITE
                R.id.xtimesRadioButton -> RepetitionEndStrategy.N_TIMES
                R.id.untilRadioButton -> RepetitionEndStrategy.UNTIL_DATE
                else -> throw Exception("Unknown repetitionEndStrategy radio id: " + binding.repetitionDetails2.checkedRadioButtonId)
            }

            val repetitionEndStrategyData = when (binding.repetitionDetails2.checkedRadioButtonId) {
                R.id.infiniteRadioButton -> RepetitionEndStrategyData.INFINITE()

                R.id.xtimesRadioButton -> {
                    val text = binding.editTextNumberRepetitionXTimes.text
                    if (text.isNullOrEmpty()) {
                        binding.editTextNumberRepetitionXTimes.error =
                            "Enter a number larger than 0"
                        return
                    }

                    val n = Integer.parseInt(text.toString())
                    if (n < 1) {
                        binding.editTextNumberRepetitionXTimes.error =
                            "Enter a number larger than 0"
                        return
                    }

                    RepetitionEndStrategyData.N_TIMES(n)
                }

                R.id.untilRadioButton -> {
                    if (selectedEndDate == null) {
                        showError("Please, select end date")
                        return
                    }
                    RepetitionEndStrategyData.UNTIL_DATE(selectedEndDate!!)
                }

                else -> throw Exception("Unknown repetitionEndStrategy radio id: " + binding.repetitionDetails2.checkedRadioButtonId)
            }

            dose.setEndData(repetitionEndStrategyData)
        }

        println("Saving dose")
        println(dose.toString())
        println("Medicine:")
        println(medicine.toString())


        if (dose.id.isNotEmpty())
            findNavController().previousBackStackEntry?.savedStateHandle?.set("doseEdit",
                dose.id to dose)
        else
            findNavController().previousBackStackEntry?.savedStateHandle?.set("dose",
                dose.id to dose)
        findNavController().navigateUp()
    }

    private fun showError(
        text: String,
        color: Int = resources.getColor(R.color.material_red_300,
            resources.newTheme()),
        fontColor: Int = resources.getColor(com.google.android.gms.base.R.color.common_google_signin_btn_text_dark,
            resources.newTheme()),
    ) {
        Objects.requireNonNull(binding)
        Snackbar.make(requireContext(),
            binding!!.root,
            text,
            Snackbar.LENGTH_LONG)
            .setBackgroundTint(color)
            .setTextColor(fontColor)
            .show()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param medicine Medicine whose dose is edited
         * @param dose The dose to edit
         * @return A new instance of fragment DoseEditFragment.
         */
        @JvmStatic
        fun newInstance(medicine: Medicine, dose: Dose? = null) =
            DoseEditFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(ARG_MEDICINE, medicine)
                    putSerializable(ARG_DOSE, dose)
                    dose?.id?.let {
                        println("EDIT STARTED OF")
                        println(dose)
                        println("AT INDEX")
                        println(it)
                    }
                }
            }
    }
}