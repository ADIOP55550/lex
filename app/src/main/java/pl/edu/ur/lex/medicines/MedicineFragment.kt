package pl.edu.ur.lex.medicines

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import pl.edu.ur.lex.R
import pl.edu.ur.lex.firebase.MedicineManager

/**
 * A fragment representing a list of Items.
 */
class MedicineFragment : Fragment() {

    private var columnCount = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val view = inflater.inflate(R.layout.medicine_fragment_item_list, container, false)

        // get the list of medicines
        MedicineManager.findAll()
            .addOnCompleteListener {
                if (view is RecyclerView) {
                    with(view) {
                        layoutManager = when {
                            columnCount <= 1 -> LinearLayoutManager(context)
                            else -> GridLayoutManager(context, columnCount)
                        }
                    }
                }
            }
            .addOnSuccessListener {
                if (view is RecyclerView) view.adapter = MedicineRecyclerViewAdapter(it.toList())
            }
            .addOnFailureListener {
                it.printStackTrace()
                if (view is RecyclerView) view.adapter = MedicineRecyclerViewAdapter(listOf())
            }

        return view
    }

    companion object {

        const val ARG_COLUMN_COUNT = "column-count"

        @JvmStatic
        fun newInstance(columnCount: Int) =
            MedicineFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }
}