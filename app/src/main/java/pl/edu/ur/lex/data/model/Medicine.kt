package pl.edu.ur.lex.data.model

import java.io.Serializable

data class Medicine(
    var id: String = "",
    var name: String = "",
    var description: String = "",
    var doses: MutableMap<String, Dose> = mutableMapOf(),
) : Serializable
