package pl.edu.ur.lex.data.model

import com.google.firebase.database.Exclude
import java.io.Serializable
import java.text.DateFormat
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.Period
import java.time.format.DateTimeFormatter
import java.time.format.TextStyle
import java.util.*

typealias DoseId = String

data class Dose(
    var timeBegin: Time = Time.MIDNIGHT,
    var dateBegin: Date = Date(),
    var repeats: Boolean = false,
    var id: DoseId = "",
    var repetitionStrategy: RepetitionStrategy = RepetitionStrategy.EVERYDAY,
    var _repetitionStrategyData: Any? = null,
    var repetitionEndStrategy: RepetitionEndStrategy = RepetitionEndStrategy.INFINITE,
    var _repetitionEndStrategyData: Any? = null,
    var finishedAllDoses: Boolean = false,
    var taken: MutableList<Date> = mutableListOf(),
) : Serializable {

    @Exclude
    private lateinit var _medicine: Medicine

    var medicine: Medicine
        @Exclude
        get() = _medicine
        @Exclude
        set(value) {
            _medicine = value
        }

    fun getRepetitionAsString(): String {
        if (!repeats)
            return "Does not repeat"
        return "Repeats " + when (repetitionStrategy) {
            RepetitionStrategy.WEEKDAYS -> {
                val list = (getData() as RepetitionStrategyData.WEEKDAYS).value
                "every " + list.joinToString { d ->
                    d.getDisplayName(TextStyle.SHORT,
                        Locale.getDefault())
                }
            }
            RepetitionStrategy.EVERY_N_PERIODS -> {
                val pair = (getData() as RepetitionStrategyData.EVERY_N_PERIODS).value
                "every " + pair.first + " " + pair.second.description + (if (pair.first > 1) "s" else "")
            }
            RepetitionStrategy.EVERYDAY -> "everyday"
        } + " " + when (repetitionEndStrategy) {
            RepetitionEndStrategy.INFINITE -> "forever"
            RepetitionEndStrategy.N_TIMES -> ((getEndData() as RepetitionEndStrategyData.N_TIMES).value).toString() + " time(s)"
            RepetitionEndStrategy.UNTIL_DATE -> "until " + DateFormat.getDateInstance()
                .format((getEndData() as RepetitionEndStrategyData.UNTIL_DATE).value)
        }
    }

    fun getTimesAfterOrEqual(date: LocalDate): Sequence<LocalDateTime> {
        return getTimes(date, false)
    }

    fun getTimesBeforeOrEqual(date: LocalDate): Sequence<LocalDateTime> {
        return getTimes(date, true)
    }

    private fun getTimes(date: LocalDate, before: Boolean = false): Sequence<LocalDateTime> {

        println("Dose.getTimes")
        println("date = [${date}], before = [${before}]")

        @Suppress("DEPRECATION")
        var current = LocalDateTime.of(
            dateBegin.year + 1900,
            dateBegin.month + 1,
            dateBegin.date,
            timeBegin.hour,
            timeBegin.minute
        )
        val target = LocalDateTime.from(date.atStartOfDay())

        if (!repeats)
            return if ((before && current <= target) || (!before && current >= target)) sequenceOf(
                current) else sequenceOf()

        println("Sequence repeats so we enter advanced phase")

        val repeatPeriodGenerator: (LocalDateTime) -> Period = getRepeatPeriodGenerator(before)

        if (!firstDayMeetsWeekdayCriteria(current.toLocalDate()))
            current = current.plus(repeatPeriodGenerator.invoke(current))

        println("First day met criteria")

        var counter = 0
        val test: ((LocalDateTime, LocalDateTime) -> Boolean) =
            if (before) { _current, _target -> _current >= _target } else { _current, _target -> _current <= _target }

        println("current now: " + current.format(DateTimeFormatter.ISO_DATE_TIME))
        while (test(current, target)) {
            println("Moving current to target:")
            current = current.plus(repeatPeriodGenerator.invoke(current))
            counter++
            println("current now: " + current.format(DateTimeFormatter.ISO_DATE_TIME))
        }

        println("Moving current finished")
        println("current now: " + current.format(DateTimeFormatter.ISO_DATE_TIME))

        @Suppress("DEPRECATION")
        val finished: (LocalDate) -> Boolean = { date ->
            println("Dose.getTimes.finished")
            println("date = [${date}]")
            when (repetitionEndStrategy) {
                RepetitionEndStrategy.INFINITE -> false
                RepetitionEndStrategy.N_TIMES -> counter++ >= (getEndData() as RepetitionEndStrategyData.N_TIMES).value
                RepetitionEndStrategy.UNTIL_DATE -> date >= (getEndData() as RepetitionEndStrategyData.UNTIL_DATE).value.let {
                    LocalDate.of(
                        it.year + 1900,
                        it.month + 1,
                        it.date
                    )
                }
            }
        }

        return sequence {
            println("Entering sequence")
            println("current now: " + current.format(DateTimeFormatter.ISO_DATE_TIME))
            while (!finished(current.toLocalDate())) {
                println("Yielding: " + current.format(DateTimeFormatter.ISO_DATE_TIME))
                yield(current)

                println("After yield")
                // move to the next possible date
                current = current.plus(repeatPeriodGenerator.invoke(current))
            }
        }


//        return generateSequence(current) { current.plus(repeatPeriodGenerator.invoke(current)) }
    }

    private fun getRepeatPeriodGenerator(backwards: Boolean = false): (d: LocalDateTime) -> Period {
        if (backwards) {
            return when (repetitionStrategy) {
                RepetitionStrategy.EVERYDAY -> { _ -> Period.ofDays(-1) }

                RepetitionStrategy.WEEKDAYS -> { d ->
                    val days = (getData() as RepetitionStrategyData.WEEKDAYS).value
                    var curr = d.dayOfWeek.minus(1)
                    var offset = -1

                    while (!days.contains(curr)) {
                        curr = curr.minus(1)
                        offset--
                    }

                    Period.ofDays(offset)
                }

                RepetitionStrategy.EVERY_N_PERIODS -> { _ ->
                    (getData() as RepetitionStrategyData.EVERY_N_PERIODS).value.let {
                        when (it.second) {
                            RepetitionPeriod.DAY -> Period.ofDays(-it.first)
                            RepetitionPeriod.WEEK -> Period.ofWeeks(-it.first)
                            RepetitionPeriod.MONTH -> Period.ofMonths(-it.first)
                            RepetitionPeriod.YEAR -> Period.ofYears(-it.first)
                        }
                    }
                }
            }
        }

        return when (repetitionStrategy) {
            RepetitionStrategy.EVERYDAY -> { _ -> Period.ofDays(1) }

            RepetitionStrategy.WEEKDAYS -> { d ->
                val days = (getData() as RepetitionStrategyData.WEEKDAYS).value
                var curr = d.dayOfWeek.plus(1)
                var offset = 1

                while (!days.contains(curr)) {
                    curr = curr.plus(1)
                    offset++
                }

                Period.ofDays(offset)
            }

            RepetitionStrategy.EVERY_N_PERIODS -> { _ ->
                (getData() as RepetitionStrategyData.EVERY_N_PERIODS).value.let {
                    when (it.second) {
                        RepetitionPeriod.DAY -> Period.ofDays(it.first)
                        RepetitionPeriod.WEEK -> Period.ofWeeks(it.first)
                        RepetitionPeriod.MONTH -> Period.ofMonths(it.first)
                        RepetitionPeriod.YEAR -> Period.ofYears(it.first)
                    }
                }
            }
        }
    }

    private fun firstDayMeetsWeekdayCriteria(localDate: LocalDate): Boolean {
        if (repetitionStrategy == RepetitionStrategy.EVERYDAY)
            return true
        if (repetitionStrategy == RepetitionStrategy.EVERY_N_PERIODS)
            return true
        if (repetitionStrategy == RepetitionStrategy.WEEKDAYS)
            return (getData() as RepetitionStrategyData.WEEKDAYS).value.contains(localDate.dayOfWeek)

        throw IllegalStateException()
    }


    @Suppress("UNCHECKED_CAST")
    @Exclude
    fun getData(): RepetitionStrategyData {
        return when (repetitionStrategy) {
            RepetitionStrategy.EVERYDAY -> RepetitionStrategyData.EVERYDAY()
            RepetitionStrategy.WEEKDAYS -> RepetitionStrategyData.WEEKDAYS(_repetitionStrategyData as List<DayOfWeek>)
            RepetitionStrategy.EVERY_N_PERIODS -> RepetitionStrategyData.EVERY_N_PERIODS(
                _repetitionStrategyData as Pair<Int, RepetitionPeriod>)
        }
    }

    @Suppress("UNCHECKED_CAST")
    @Exclude
    fun getEndData(): RepetitionEndStrategyData {
        return when (repetitionEndStrategy) {
            RepetitionEndStrategy.INFINITE -> RepetitionEndStrategyData.INFINITE()
            RepetitionEndStrategy.N_TIMES -> RepetitionEndStrategyData.N_TIMES(
                _repetitionEndStrategyData as Int)
            RepetitionEndStrategy.UNTIL_DATE -> RepetitionEndStrategyData.UNTIL_DATE(
                _repetitionEndStrategyData as Date)
        }
    }

    @Exclude
    fun setData(data: RepetitionStrategyData?) {
        _repetitionStrategyData = data?.value
    }

    @Exclude
    fun setEndData(data: RepetitionEndStrategyData?) {
        _repetitionEndStrategyData = data?.value
    }

}

@Suppress("ClassName")
sealed class RepetitionStrategyData {
    abstract val value: Any?

    data class EVERYDAY(override val value: Void? = null) : RepetitionStrategyData()
    data class WEEKDAYS(override val value: List<DayOfWeek>) : RepetitionStrategyData()
    data class EVERY_N_PERIODS(override val value: Pair<Int, RepetitionPeriod>) :
        RepetitionStrategyData()
}

@Suppress("ClassName")
sealed class RepetitionEndStrategyData {
    abstract val value: Any?

    data class INFINITE(override val value: Void? = null) : RepetitionEndStrategyData()
    data class N_TIMES(override val value: Int) : RepetitionEndStrategyData()
    data class UNTIL_DATE(override val value: Date) : RepetitionEndStrategyData()
}

enum class RepetitionStrategy : Serializable {
    EVERYDAY,
    WEEKDAYS,
    EVERY_N_PERIODS,
}

enum class RepetitionPeriod(val description: String) : Serializable {
    DAY("day"),
    WEEK("week"),
    MONTH("month"),
    YEAR("year"),
}

enum class RepetitionEndStrategy : Serializable {
    INFINITE,
    N_TIMES,
    UNTIL_DATE,
}
