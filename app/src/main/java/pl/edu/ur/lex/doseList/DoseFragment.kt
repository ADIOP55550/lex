package pl.edu.ur.lex.doseList

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import pl.edu.ur.lex.R
import java.util.*

/**
 * A fragment representing a list of Items.
 */
class DoseFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val view = inflater.inflate(R.layout.fragment_dose_list, container, false)

        // Set the adapter
        if (view is RecyclerView) {
            with(view) {
                layoutManager = LinearLayoutManager(context)
                adapter = DoseListAdapter(listOf(), Date())
            }
        }
        return view
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            DoseFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}