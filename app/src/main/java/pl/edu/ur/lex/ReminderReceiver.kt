package pl.edu.ur.lex

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast
import java.util.*

class ReminderReceiver : BroadcastReceiver() {

    private val titleExtra = "titleExtra"
    private val messageExtra = "messageExtra"

    override fun onReceive(context: Context, intent: Intent) {

        Toast.makeText(context, intent.getStringExtra(titleExtra), Toast.LENGTH_SHORT).show()
        NotificationHelper(context)
            .scheduleNotification(intent.getStringExtra(titleExtra).toString(),
                intent.getStringExtra(messageExtra).toString(),
                Calendar.getInstance().get(Calendar.HOUR_OF_DAY),
                Calendar.getInstance().get(Calendar.MINUTE) + 1)

    }

}